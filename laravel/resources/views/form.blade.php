<!DOCTYPE html>
<html>
    <head>
        <title>Buat Akun Baru!</title>
    </head>

    <body>
        <h2> Buat Account Baru! </h2>
        <h4> Sign Up Form </h4>

        <!-- membuat form -->
        <form action="/welcome" method="POST">
            @csrf
            <label for="first-name"> First Name: </label>
            <br>
            <br>
            <input type="text" id="first-name" name="first-name" class="form-control" autofocus required>
            <br>
            <br>
            <label for="last-name"> Last Name: </label>
            <br>
            <br>
            <input type="text" id="last-name" name="last-name" class="form-control" autofocus required> 
            <br>
            <br>

            <label>Gender:</label>
            <br>
            <br>

            <input type="radio" name="gender">Male<br>
            <input type="radio" name="gender">Female<br>
            <input type="radio" name="gender">Other<br>
            <br>

            <label>Nationality:</label>
            <br>
            <br>
            <select>
                <option>Indonesian</option>
                <option>Singaporean</option>
                <option>Malaysian</option>
                <option>Australian</option>
            </select>
            <br>
            <br>

            <label>Language Spoken:</label>
            <br>
            <br>
            <input type="checkbox"> Bahasa Indonesia <br>
            <input type="checkbox"> English <br>
            <input type="checkbox"> Other <br>
            <br>

            <label for="bio"> Bio: </label> <br> <br>
            <textarea cols="35" rows="10" id="bio"></textarea> <br>
            <input type="submit" value="Sign Up">

        </form>
    </body>
</html>