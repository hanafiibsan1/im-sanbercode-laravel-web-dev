<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('form');
    }

    public function welcome(Request $request) {
        $firstName = $request->input('first-name');
        $lastName = $request->input('last-name');

        return view("welcome",
                    [
                        'firstName' => $firstName,
                        'lastName' => $lastName,
                    ]);
    }  
}
