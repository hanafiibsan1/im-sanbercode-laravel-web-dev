<?php

use App\Http\Controllers\CastController;
use App\Http\Controllers\HomeController; 
use App\Http\Controllers\AuthController; 
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);

Route::get('/register', [AuthController::class, 'register']);
Route::post('/welcome', [AuthController::class, 'welcome']);

Route::get('/master', function(){
    return view('layout.master');
});

Route::get('/table', function () {
    return view('layout.table');
});

Route::get('/data-table', function () {
    return view('layout.data-table');
});


Route::get('/cast/create', [CastController::class, 'create']);
Route::post('/cast', [CastController::class, 'store']);
Route::get('/cast/{cast_id}', [CastController::class, 'show']);
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);

Route::get('/cast', [CastController::class, 'index']);