@extends('layout.master')

@section('judul')
Halaman Detail Cast
@endsection

@section('content')

<h1>{{$cast->nama}}</h1>
<br/>
<p>{{$cast->bio}}</p>

<a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>


@endsection