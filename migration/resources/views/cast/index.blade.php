@extends('layout.master')

@section('judul')
Halaman List Cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary btn-sm mb"> Tambah Cast </a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Action</th>
        {{-- <th scope="col">Umur</th>
        <th scope="col">Bio</th> --}}
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $value)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$value->nama}}</td>
                <td>
                    <a href="/cast/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                </td>
                {{-- <td>{{$value->umur}}</td>
                <td>{{$value->bio}}</td> --}}
            </tr>
        @empty
            <tr>
                <td>Tidak Ada Data</td>
            </tr>
        @endforelse
    </tbody>
  </table>


@endsection