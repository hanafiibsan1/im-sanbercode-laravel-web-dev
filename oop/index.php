<?php
require_once 'animal.php';
require_once 'frog.php';
require_once 'ape.php';

//release0
$sheep = new Animal("shaun");

echo "Name : "          . $sheep->name . "<br>"; // "shaun"
echo "legs : "          . $sheep->legs . "<br>"; // 4
echo "cold blooded : "   . $sheep->cold_blooded . "<br> <br>"; // no
 
//release1    
$kodok = new frog("buduk");
echo "name : " . $kodok->name . "<br>"; 
echo "legs : " . $kodok->legs . "<br>"; 
echo "cold blooded : " . $kodok->cold_blooded . "<br>";
echo "Jump : ";

$kodok->jump();  
echo "<br><br>";

$sungokong = new Ape("kera sakti");
echo "Name : " . $sungokong->name . "<br>"; // kera sakti
echo "legs : " . $sungokong->legs . "<br>"; // 2
echo "cold blooded : " . $sungokong->cold_blooded . "<br>";
echo "Yell : ";
 
$sungokong->yell();


?>
