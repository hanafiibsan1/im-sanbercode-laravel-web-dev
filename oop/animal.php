<?php
class Animal {
  public $name;
  public $cold_blooded;
  public $legs;
  
  
  function __construct($name,$legs = 4, $cold_blooded = false)
  {
    $this->name = $name;
    $this->legs = $legs;
    
    if($cold_blooded){
      $this->cold_blooded = "yes"; 
    }
    else {
      $this->cold_blooded = "no";
    }
}
}

?>
